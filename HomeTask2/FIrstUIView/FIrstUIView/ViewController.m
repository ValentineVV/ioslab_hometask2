//
//  ViewController.m
//  FIrstUIView
//
//  Created by Valiantsin Vasiliavitski on 3/31/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *ibTextField;
@property (weak, nonatomic) IBOutlet UIButton *ibButton;
@property (weak, nonatomic) IBOutlet UILabel *ibLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ibaAddText:(UIButton *)sender {
    self.ibLabel.text = [self.ibLabel.text stringByAppendingString:self.ibTextField.text];
}


@end
